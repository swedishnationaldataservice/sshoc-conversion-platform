<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:math="http://www.w3.org/2005/xpath-functions/math"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	xmlns:array="http://www.w3.org/2005/xpath-functions/array"
	exclude-result-prefixes="#all"
	version="3.0">

  <xsl:mode on-no-match="shallow-copy"/>

  <xsl:output method="html" indent="yes" html-version="5"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>.NET XSLT XSLT example</title>
      </head>
      <body>
        <xsl:apply-templates/>
      </body>
    </html>
    
    <xsl:comment>Test of XSLT 3.0, processor <xsl:value-of select="system-property('xsl:vendor')"/>
      <xsl:choose>
        <xsl:when test="number(system-property('xsl:version')) > 1">
          <xsl:value-of select="concat(' Product ', system-property('xsl:product-name'), ' ', system-property('xsl:product-version'))"/>
        </xsl:when>
        <xsl:when test="system-property('msxml:version')" xmlns:msxml="urn:schemas-microsoft-com:xslt">
          <xsl:text> </xsl:text>
          <xsl:value-of select="system-property('msxml:version')"/>
        </xsl:when>
      </xsl:choose>
    </xsl:comment>
  </xsl:template>
  
</xsl:stylesheet>