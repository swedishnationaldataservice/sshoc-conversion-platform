using System.IO;
using System;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using SshocConversionPlatform.Models;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace SshocConversionPlatform.Services
{
    public class TransformationMetadataService
    {
        private readonly IConfiguration Configuration;

        public TransformationMetadataService(IConfiguration configuration){
            Configuration = configuration;
        }
      
      public bool XsltHaveMetadata(string path){
          XNamespace xmlns = "https://github.com/MetadataTransform";
          XElement xslt = XElement.Load(path);
          if(xslt.Descendants(xmlns + "metadata").Any()){
            return true;
          }
          return false;
      }

      public bool isValidTrasnformationIdentifier(string transormationId){
        if(this.getTranformationMetadata(transormationId) != null){
          return true;
        }

        return false;
      }

      public TranformationMetadata? getTranformationMetadata(string transormationId){
        IEnumerable<TranformationMetadata> tranformations = this.getTransformations();

        foreach(TranformationMetadata transformation in tranformations){
          if(transformation.Identifier == transormationId){
            return transformation;
          }
        }

        return null;
      }

      public IEnumerable<TranformationMetadata> getTransformations(){
        string[] files = Directory.GetFiles(Configuration["XsltDirectory"], "*.xslt", SearchOption.AllDirectories);
        List<TranformationMetadata> list = new List<TranformationMetadata>();
        
        foreach(string file in files){
          list.Add(new TranformationMetadata(file));
        }

        return list;
      }
        
    }
}