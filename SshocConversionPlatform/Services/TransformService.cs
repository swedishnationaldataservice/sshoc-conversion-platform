using System.IO;
using System.Diagnostics;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace SshocConversionPlatform.Services
{
    public class TransformService
    {
        private readonly IConfiguration Configuration;

        public TransformService(IConfiguration configuration){
            Configuration = configuration;
        }

        public async Task<string> callSaxon(string xmlPath, string xsltPath)
        {
            string result = "";
            string saxonPath = Configuration["SaxonPath"];
            System.Console.WriteLine($"xmlPath: {xmlPath}");
            System.Console.WriteLine($"saxonPath: {saxonPath}");

            using (var process = new Process())
            {
                System.Console.WriteLine("Setup...");
                process.StartInfo.FileName = "java";
                process.StartInfo.Arguments = String.Format("-jar {0} {1} {2}", saxonPath, xmlPath, xsltPath);

                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                process.OutputDataReceived += (sender, data) => System.Console.WriteLine(data.Data);
                process.ErrorDataReceived += (sender, data) => System.Console.WriteLine(data.Data);
                System.Console.WriteLine("starting");
                process.Start();
                
                result = await process.StandardOutput.ReadToEndAsync();
                
                var exited = process.WaitForExit(1000 * 10);
                System.Console.WriteLine($"exit {exited}");
            }
            return result;
        }
    }
}