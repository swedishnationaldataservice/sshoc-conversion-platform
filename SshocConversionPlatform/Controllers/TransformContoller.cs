using System.IO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using SshocConversionPlatform.Services;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using SshocConversionPlatform.Models;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace SshocConversionPlatform.Controllers
{
    public class TransformController : Controller
    {   
        TransformService transformService;
        TransformationMetadataService transformationMetadataService;
        private readonly IConfiguration Configuration;


        public TransformController(IConfiguration configuration){
            this.transformService = new TransformService(configuration);
            this.transformationMetadataService = new TransformationMetadataService(configuration);
            Configuration = configuration;
        }

        [HttpGet("/transformation-list")]
        public IEnumerable<TranformationMetadata> TransformationList(){
            return this.transformationMetadataService.getTransformations();
        }

        [HttpPost("/transform/{transformationId}")]
        public async Task<IActionResult> Transform(string transformationId)
        {
            var inputStream = new StreamReader(Request.Body);

            TranformationMetadata transformation = this.transformationMetadataService.getTranformationMetadata(transformationId);
            
            try
            {
                var fullPath = Path.GetTempFileName();
                System.Console.WriteLine("Gettings tmp file...");
                
                using(FileStream outputFileStream = new FileStream(fullPath, FileMode.Create)) {  
                    await inputStream.BaseStream.CopyToAsync(outputFileStream);
                    outputFileStream.Close();
                }

                string fileContent = System.IO.File.ReadAllText(fullPath);
                System.Console.WriteLine($"temp filename: {fullPath}");

                var res = await this.transformService.callSaxon(fullPath, transformation.Path);
                System.IO.File.Delete(fullPath);
                
                return Ok(res);
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
        
    }
}