using Microsoft.AspNetCore.Mvc;

namespace SshocConversionPlatform.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet("/")]
        public string Index()
        {
            return "SSHOC conversion platform (dev)";
        }
    }
}