using System.IO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using SshocConversionPlatform.Services;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SshocConversionPlatform.Models{

    public class TranformationMetadata{

        public string Identifier{get;set;}
        public string Path{get;set;}
        public string Title{get;set;}
        public string Description{get;set;}
        public string OutputFormat{get;set;}
        public Parameter[] Parameters{get;set;}

        public TranformationMetadata(XElement element){
            this.readMetadata(element);
        }

        public TranformationMetadata(string path){
            this.Path = path;
            XElement xslt = XElement.Load(path);
            this.readMetadata(xslt);
        }

        private void readMetadata(XElement element){
            Identifier = (string)element.Descendants("identifier").FirstOrDefault();
            Title = (string)element.Descendants("title").FirstOrDefault();
            Description = (string)element.Descendants("description").FirstOrDefault();
            OutputFormat = (string)element.Descendants("outputFormat").FirstOrDefault();
            Parameters = new Parameter[0];
        }
    }

    public class Parameter{
        public string Name{get;set;}
        public string Description{get;set;}
        public string Format{get;set;}
    }
}