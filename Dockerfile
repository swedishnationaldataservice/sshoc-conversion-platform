FROM mcr.microsoft.com/dotnet/sdk:5.0.202 AS build
WORKDIR /source

USER root 

# copy csproj and restore as distinct layers
COPY *.sln .
COPY SshocConversionPlatform/*.csproj ./SshocConversionPlatform/
RUN dotnet restore

# copy everything else and build app
COPY SshocConversionPlatform/. ./SshocConversionPlatform/
WORKDIR /source/SshocConversionPlatform
RUN dotnet publish -c release -o /app

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:5.0

EXPOSE 8051
ENV ASPNETCORE_URLS=http://+:8051

# copy jre
COPY --from=gradle:jdk11 /opt/java /opt/java
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH="$PATH:$JAVA_HOME/bin"

# Add a new user "sshoc-dev" with user id 8877
RUN useradd -u 8877 sshoc-dev

# get saxon
ARG SAXON_VERSION=10.3
ADD \
  https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/${SAXON_VERSION}/Saxon-HE-${SAXON_VERSION}.jar \
  /utils/saxon.jar

# set owner rights and X to saxon.jar for sshoc-dev
RUN chown sshoc-dev /utils/saxon.jar
RUN chmod a+x /utils/saxon.jar

# get ddi-xslt
ADD \
  https://github.com/MetadataTransform/ddi-xslt/releases/download/dev-0.4/ddi-xslt.dev-0.4.zip \
  /xslt/ddi-xslt.dev.zip
  
#Install unzip
RUN apt-get update && apt-get install -y unzip

#Unzip and delete compressed ddi-xslt
WORKDIR /xslt
RUN unzip /xslt/ddi-xslt.dev.zip && \
    rm /xslt/ddi-xslt.dev.zip

# add saxon as alias
RUN echo -e '#!/bin/bash\njava -jar /utils/saxon.jar "$@"' > /usr/bin/saxon && \
    chmod +x /usr/bin/saxon

WORKDIR /app
COPY --from=build /app ./

# TEST
ENV JAVA_APP_JAR /utils/saxon.jar

RUN chgrp -R 0 /utils/ && \
    chmod -R g=u /utils/

RUN chgrp -R 0 /utils/saxon.jar && \
    chmod -R g=u /utils/saxon.jar

RUN chgrp -R 0 /tmp/ && \
    chmod -R g=u /tmp/

RUN chgrp -R 0 /xslt/ && \
    chmod -R g=u /xslt/
# END TEST

COPY Xslt /xslt

# Change to non-root privilege
USER sshoc-dev


ENTRYPOINT ["dotnet", "SshocConversionPlatform.dll"]